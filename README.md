# Processo Seletivo 2020 #

## Menor Preço ##

O objetivo da avaliação é construir uma API (REST/JSON) para consulta e recuperação de  produtos, filtrada pelo código GTIN (EAN) e ordenada de forma crescente pelo preço.
Você deve utilizar a base de dados de produtos *fake* disponibilizada ao final da descrição.

1. Crie uma modelagem de banco de dados para o *dataset* fornecido utilizando banco de dados *sqlite*. Elabore uma modelagem que seja a mais adequada para a solução proposta.
2. Crie um mecanismo para realizar a importação dos dados do arquivo com extensão *.csv* para o *.sqlite* de forma dinâmica. Ex: uma rota (*GET* /v1/importar)
3. Defina uma rota **GET /v1/produtos** para obter todos os produtos contidos no banco de dados. A rota deve receber como parâmetro um valor GTIN. Caso o acesso a esse *endpoint* seja sem esse parâmetro, o sistema deve retornar resposta 400 (Bad Request). Caso o produto não exista, retornar 404 (Not Found).
4. Criar um mecanismo de validação para verificar a existência ou não dos parâmetros obrigatórios da API.

A localização (latitude e longitude) de cada contribuinte, associado a cada produto, está disponível no dataset, respectivamente nas colunas cod_latitude e cod_longitude. Uma consulta por produto (código GTIN) ao endpoint deve resultar em uma listagem contendo o último produto vendido de cada estabelecimento, ordenada de forma crescente pelo preço. Os registros que não possuírem preço ou cujo contribuinte não possua localização devem ser descartados. Retornar, também, na listagem acima a URL do Google Maps contendo o endereço do estabelecimento.

> Para participar crie um repositório público (GIT) e envie e-mail para **processo-seletivo-dev@sefaz.es.gov.br** contendo seu nome completo, currículo em anexo e a URL do repositório.

> A tarefa deve ser implementada em .NET 5.

> Crie arquivo README.md contendo instrução passo-a-passo para instalação e execução da sua aplicação.

> Implementações fora desse contexto não serão pontuadas.

### O que será avaliado ###
1. Formato da entrega.
2. Arquivo README.md com instruções para executar sua aplicação.
3. Execução do roteiro acima e funcionamento da sua aplicação.
4. Aderência ao contexto informado.
5. Modelagem e importação dos dados
6. Rota /v1/produtos
7. Validação dos parâmetros obrigatórios
8. Os diferenciais serão critérios de desempate

### Diferenciais ###

* Criar um mecanismo que realize um log de cada request, registrando o horário, o GTIN, o status code da requisição e o número de produtos retornados. Decida se o log será registrado em banco de dados ou em arquivo texto.
* Escrever testes unitários que garantam o funcionamento esperado da API e cubram todo o código.
* Permitir informar a sua latitude e longitude na rota de produtos (parâmetro opcional) para simular sua localização e obter no retorno da API um atributo *distância* (em KM) de você até o endereço do contribuinte. Este atributo não deve pertencer à modelagem de banco de dados, uma vez que se trata de um atributo calculado em tempo real, de acordo com a localização informada e a localização de cada contribuinte.

### Cargo ###

* Salário: R$2.535,28
* Auxílio alimentação: R$18,00 por dia útil
* Plano de saúde participativo

### Dataset ###
[dataset](https://internet.sefaz.es.gov.br/downloads/arquivos/dataset-processo-seletivo-2019.zip)